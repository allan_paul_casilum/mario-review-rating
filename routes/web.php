<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('index');
Route::get('/', 'HomeController@index')->name('index');
Route::get('product/{id}', 'ProductController@show')->name('showproduct');

Route::group(['middleware' => 'auth'], function () {
	Route::get('product/review/approve/{product_id}/{id}', 'ProductController@approve')->name('approvereview');
	Route::get('product/review/remove/{product_id}/{id}', 'ProductController@destroy')->name('deletereview');
});

Route::get('logout',function(){
	Auth::logout();
	return redirect('/login');
});
/*Route::group(['prefix' => 'admin'], function () {
    Route::get('users', function ()    {
    });
});*/
//Route::get('/home', 'HomeController@index');


