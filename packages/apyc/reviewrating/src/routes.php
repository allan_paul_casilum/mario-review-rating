<?php
Route::get('reviewrating', function(){
	echo 'Hello from the reviewrating package!';
});
Route::get('add/{a}/{b}', 'Apyc\ReviewRating\Controller\ReviewRatingController@add');
Route::post('product/ajax-add-rating-review', 'Apyc\ReviewRating\Controller\ReviewRatingController@ajaxAddRatingReview');
