/* Javascript */
 
$(function () {
	var $getrateYo = $('.getrateYo').rateYo();
	
	var $rateYo = $(".rateYo").rateYo({
		fullStar: true
	});
	
	$('.getRating').on('click',function(e){
		/* get rating */
		
		var rating = $rateYo.rateYo("rating");
		//console.log(rating);
		var _full_name = $('.fullname').val();
		var _review = $('.review').val();
		var _product_id = $('.product_id').val();
		
		var data = {
			'rating':rating,
			'name':_full_name,
			'msg_review':_review,
			'product_id':_product_id
		};
		var request = $.ajax({
			url: "ajax-add-rating-review",
			method: "POST",
			data: data,
			dataType: "json"
		});
		request.done(function( msg ) {
		  console.log(msg);
		  location.reload();
		});
	});
});
