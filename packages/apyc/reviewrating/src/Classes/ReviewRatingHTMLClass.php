<?php
namespace Apyc\ReviewRating\Classes;

/**
 * Use to display the star rating and related
 * */
class ReviewRatingHTMLClass{
	/**
	 * instance of this class
	 *
	 * */
	protected static $instance = null;

	/**
	 * Return an instance of this class.
	 *
	 * @return    object    A single instance of this class.
	 */

	public static function get_instance() {

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}
	
	/**
	 * display the star rating
	 * */
	public function starRating(){
		$data = array();
		echo view('reviewrating::addstar', $data);
	}

	/**
	 * display the star rating
	 * */
	public function getStarRating($data = array()){
		echo view('reviewrating::getstar', $data);
	}
	
	public function __construct(){
	}
}
