<?php
namespace Apyc\ReviewRating\Classes;
/**
 * This is use to include script, javascript and css
 * */
class ReviewRatingScriptClass{
	/**
	 * instance of this class
	 *
	 * */
	protected static $instance = null;

	/**
	 * Return an instance of this class.
	 *
	 * @return    object    A single instance of this class.
	 */

	public static function get_instance() {

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}
	
	/**
	 * include for JS
	 * */
	public function JS(){
		?>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"></script>
		<script src="<?php echo asset("/js/reviewrating.js"); ?>"></script>
		<?php
		
	}
	
	/**
	 * include for CSS
	 * */
	public function CSS(){
		?>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">
		<?php
	}
	
	public function __construct(){
	}
}
