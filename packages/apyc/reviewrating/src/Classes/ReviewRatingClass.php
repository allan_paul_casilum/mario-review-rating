<?php
namespace Apyc\ReviewRating\Classes;

class ReviewRatingClass{
	/**
	 * instance of this class
	 *
	 * */
	protected static $instance = null;

	/**
	 * Return an instance of this class.
	 *
	 * @return    object    A single instance of this class.
	 */

	public static function get_instance() {

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}
	
	public function test(){
		echo 'test review rating class';
	}
	
	public function __construct(){
	}
}
