<?php

namespace Apyc\ReviewRating\Controller;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;
use Session;
use Apyc\ReviewRating\Model\Review;

class ReviewRatingController extends Controller
{
    public function ajaxAddRatingReview(Request $request){
		$request->request->add(['session_id' => Session::getId()]);
		$input = $request->all();
		$review = new Review;
		$review->name = $input['name'];
		$review->rating = $input['rating'];
		$review->msg_review = $input['msg_review'];
		$review->product_id = $input['product_id'];
		$review->approved = 0;
		$review->session_id = $input['session_id'];
		$saved = $review->save();
		return Response::json($saved);
	}
}
