<?php

namespace Apyc\ReviewRating;

use Illuminate\Support\ServiceProvider;

class ReviewRatingServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        include __DIR__.'/routes.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->make('Apyc\ReviewRating\Controller\ReviewRatingController');
        $this->loadViewsFrom(__DIR__.'/views', 'reviewrating');
        $this->publishes([
			__DIR__.'/assets/js' => base_path('public/js'),
		]);
    }
}
