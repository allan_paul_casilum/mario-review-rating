<?php

namespace Apyc\ReviewRating\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Review extends Model
{
	use SoftDeletes;
	
    protected $table = 'review';
	
	/**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'rating', 'msg_review', 'product_id', 'approved', 'session_id'
    ];
    
    public function product(){
        return $this->belongsTo('App\Product');
    }
    
    public function scopeApprove($query){
        return $query->where('approved', 1);
    }
}
