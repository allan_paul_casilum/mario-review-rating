<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'product';
	
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'us_states', 'us_states_code', 'license', 'expiration_date', 'valid'
    ];
	
	public function review(){
        return $this->hasMany('Apyc\ReviewRating\Model\Review');
    }
	
}
