@extends('layout')


@section('content')
<h1></h1>
<p></p>
<p></p>
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Product</h2>
            </div>
        </div>
    </div>
	
	@if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
	
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
	<ul class="list-group">
	  @foreach ($dbs as $db)
		<li class="list-group-item">
			<a href="{{route('showproduct',['id'=>$db->id])}}">{{ $db->name}}</a>
		</li>
	  @endforeach
	</ul>
@endsection
