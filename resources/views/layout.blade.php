<!DOCTYPE html>
<html>
<head>
	<title>Review</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	{{Apyc\ReviewRating\Classes\ReviewRatingScriptClass::get_instance()->CSS()}}
</head>
<body>


<div class="container">
    @yield('content')
</div>

<script
src="http://code.jquery.com/jquery-3.3.1.min.js"
integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
crossorigin="anonymous"></script>
{{Apyc\ReviewRating\Classes\ReviewRatingScriptClass::get_instance()->JS()}}
</body>
</html>
