@extends('layout')


@section('content')
<h1></h1>
<p></p>
<p></p>
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Product</h2>
            </div>
        </div>
    </div>
	
	@if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
	
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
	<h2>{{$product->name}}</h2>
	@foreach($product->review as $reviews)

		@if($reviews->approved == 1 || Auth::check())
			<?php Apyc\ReviewRating\Classes\ReviewRatingHTMLClass::get_instance()->getStarRating(['rating'=>$reviews->rating]);?>
			<p>Name: {{$reviews->name}}</p>
			<p>{{$reviews->msg_review}}</p>
			@if( $reviews->approved == 0 && Auth::check() )
				<a href="{{route('approvereview',['product_id'=>$product_id,'id'=>$reviews->id])}}">Approve</a> | 
			@endif
			@if( Auth::check() )
				<a href="{{route('deletereview',['product_id'=>$product_id,'id'=>$reviews->id])}}">Delete</a>
			@endif
			<p></p>
			<br>
			<hr>
		@endif
		
	@endforeach
	<br>
	<h3>Add Review and Rating</h3>
	<?php Apyc\ReviewRating\Classes\ReviewRatingHTMLClass::get_instance()->starRating();?>
	<input type="hidden" name="product_id" value="{{$product->id}}" class="product_id">
@endsection
