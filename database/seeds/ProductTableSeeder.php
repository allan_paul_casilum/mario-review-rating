<?php

use Illuminate\Database\Seeder;
//use DB;
class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product')->insert([
			['name' => 'aa'],
			['name' => 'bb'],
			['name' => 'cc'],
			['name' => 'dd'],
			['name' => 'ee'],
			['name' => 'ff'],
			['name' => 'gg'],
			['name' => 'hh'],
			['name' => 'ii'],
			['name' => 'jj'],
        ]);
    }
}
